import cosine from "~/node_modules/wink-distance";


// TODO: Read JSON FILES!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
class RecommenderSystem {
    constructor(recipes, dict_of_ingr_occurences) {
        this.recipes = recipes; // This is supposed to be json file tfidf_info.txt
        this.dict_of_word_occurences = dict_of_ingr_occurences; // This is supposed to be json file frequencies.txt
        this.num_of_recipes = recipes.length;
    }


    function

    random_recipe(recipes) {
        var i = Math.floor(Math.random() * (num_of_recipes - 1 - 0));
        return i;
    }

    /**
     *
     * @param recipes array of dictionaries containing keys "rating", "ingredients", "title"
     * @param ingredients are ingredients set build from user preferences
     * @param used_recipes recipes that have already been used by this algorithm
     * @returns return index of next recipe to recommend
     */
    function

    calculate_jaccard(recipes, ingredients, used_recipes) {
        /*
        |A intersect B| / |A union B|
        1 == equality of A and B

        Compute Jaccard coef of ingredients with all other recipes
        */
        var jaccard_coef = [];
        for (var i = 0; i < recipes.length; i++) {
            const recipe = recipes[i];
            var recipe_ingredients = [for (x of recipe["ingredients"]) x[0]]; // Want only the first value json dictionary, so key?
            var intersect = ingredients.intersection(recipe_ingredients);
            var union = ingredients.union(recipe_ingredients);

            jaccard_coef.append([intersect / union, i])
        }

        // Sort in descending way
        jaccard_coef.sort(function (a, b) {
            return b[0] - a[0]
        });

        for (i = 0; i < jaccard_coef.length; i++) {
            if (!(jaccard_coef[i][1] in used_recipes)) {
                used_recipes.append(jaccard_coef[i][1]);
                // Returning index of recipe
                return jaccard_coef[i][1];
            }
        }
    }

    /** Calculates tfidf to whole corpus precomputed in python
     *
     * @param ingredients set built from user preferences
     * @returns set of ingredients
     */
    function

    calculate_tfidf(ingredients) {
        var tfidf = {};
        const tf = 1 / ingredients.length;
        var ingr;
        for (ingr of ingredients) {
            var idf = 0;
            if (ingr in dict_of_word_occurences) {
                idf = Math.log(num_of_recipes / dict_of_word_occurences[ingr]);
            }
            tfidf[ingr] = tf * idf;
        }
        return tfidf;
    }

    /** Will be called once with rating_weight = false and once with true
     *
     * @param recipes array of dictionaries containing keys "rating", "ingredients", "title"
     * @param ingredients are ingredients set build from user preferences
     * @param used_recipes recipes that have already been used by this algorithm
     * @param rating_weight Boolean, says whether to use rating as a weighting factor to cosine similarity
     * @returns return index of next recipe to recommend
     */
    function

    calculate_cosine_similarity(recipes, ingredients, used_recipes, rating_weight = false) {
        let i;
        var results = [];
        var tfidf_ingredients = calculate_tfidf(ingredients);
        for (i = 0; i < recipes.length; i++) {
            // Cosine should work on dictionary of words, format "ingredient" : TFIDF score
            // Need tuples here because of index
            results.append([cosine(recipes[i]["ingredients"], tfidf_ingredients), i]);
        }

        // Normalize by rating
        if (rating_weight) {
            for (i = 0; i < recipes.length; i++) {
                results[i][0] *= recipes[i]["rating"]
            }
        }
        // Sort in descending way
        results.sort(function (a, b) {
            return b[0] - a[0]
        });

        for (i = 0; i < results.length; i++) {
            if (!(results[i][1] in used_recipes)) {
                used_recipes.append(results[i][1]);
                // Returning index of recipe
                return results[i][1];
            }
        }
    }

    /**
     *
     * @param ingredients that user has chosen
     */
    function

    recommend_recipes_by_ingredients(ingredients) {

    }

    /**
     *
     * @param chosen_recipe_index index of recipe that user has chosen
     * @param chosen_algorithm algorithm that was used (TODO: Enum or index?)
     */
    function

    recommend_by_chosen_recipe(chosen_recipe_index, chosen_algorithm) {
        // Need to do union on already known ingredients from "knowledge base"
        // and ingredients of the recipe
        // Then run all algorithms on this knowledge base and return recipes to front end
        // calls to individual algorithms:
        //
    }

}