#! /usr/bin/env python
# -*- coding: utf-8 -*-

class Recipe:
    '''
    Class for storing info about recipe.
    '''
    def __init__(self, rating, title, sodium, ingredients, calories, fat, directions, protein, categories, description):
        self.rating = rating
        self.title = title
        self.sodium = sodium
        self.ingredients = ingredients
        self.calories = calories
        self.fat = fat
        self.directions = directions
        self.protein = protein
        self.categories = categories
        self.description = description
        self.jaccard = None
        self.cosine = None

    def get_rating(self):
        return self.rating

    def get_title(self):
        return self.title

    def get_sodium(self):
        return self.sodium

    def get_ingredients(self):
        return self.ingredients

    def get_calories(self):
        return self.calories

    def get_fat(self):
        return self.fat

    def get_directions(self):
        return self.directions

    def get_protein(self):
        return self.protein

    def get_categories(self):
        return self.categories

    def get_description(self):
        return self.description

    def get_jaccard(self):
        return self.jaccard

    def set_ingredients(self, ingredients):
        self.ingredients = ingredients

    def set_jaccard(self, jaccard_coef):
        self.jaccard = jaccard_coef

    def get_cosine(self):
        return self.cosine

    def set_cosine(self, cosine):
        self.cosine = cosine
