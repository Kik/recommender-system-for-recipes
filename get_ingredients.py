import pandas as pd
from flashtext import KeywordProcessor

ingredients = pd.read_csv('nyt-ingredients-snapshot-2015.csv').name
f = open("ingredients.txt", "w+")
words_to_remove = ["cup", "tbsp", "tablespoon", "tablespoons", "teaspoon", "teaspoons", "sliced", "diced",\
                   'or', 'de', 'and', 'by', 'finely', 'into', 'to', 'about', 'of', 'chopped', 'thinly', \
                   'large', 'small', 'medium', 'minced', 'grated', 'toasted', '1/2', '1/4', '3/4', \
                   '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', ',', ')', '(', '&', 'freshly', 'crushed', 'fresh', 'ground', \
                   'ripe', 'squeezed', 'favorite', 'shredded', 'oz', 'whole', 'for', 'of', 'from', 'with', 'thick', 'slices', 'canned',
                   'baby', 'warm', 'boiling', 'virgin', 'water', 'nonstick', 'coarse', 'part']
kw_p = KeywordProcessor()
kw_p.add_keywords_from_list(words_to_remove)
arr = []
for i in ingredients: 
    if len(kw_p.extract_keywords(str(i))) > 0: 
        continue
    arr.append((str(i).lower()).strip())
    
interpunction = [',', ')', '(', '&', '-', '\'', '\`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '\t', '<', '>', ':']
arr = list(set(arr))
for i in arr:
    if(i.isspace()):
        continue
    if any(s in i for s in interpunction):
        continue
    if i.endswith("s"):
        continue
    f.write(i + "\r\n")  