#! /usr/bin/env python
# -*- coding: utf-8 -*-

import json
from random import randint


def load_recipes(file):
    with open(file, 'r+') as f:
        return json.loads(f.read())

def generate_random_indexes(data):
    return [randint(0, len(data) - 1) for _ in range(5)]

def get_random_recipes(data):
    rand_indexes = generate_random_indexes(data)
    while len(set(rand_indexes)) != 5:
        print ('ooo')
        rand_indexes = generate_random_indexes(data)

    return [data[i] for i in rand_indexes]

def get_jaccard_recipes(data, title):
    for rec in data:
        if rec['title'] == title:
            return rec['jaccard']  # TODO do we want to send an order of recipe or only recipes?

    return 'no match'

def get_cosine_recipes(data, title):
    for rec in data:
        if rec['title'] == title:
            return rec['cosine']  # TODO do we want to send an order of recipe or only recipes?

    return 'no match'

if __name__ == '__main__':
    recipes = load_recipes('recipes.txt')

    # print (get_random_recipes(recipes))

    # print (get_jaccard_recipes(recipes, 'Spinach Noodle Casserole '))

    print (get_cosine_recipes(recipes, 'Spinach Noodle Casserole '))
