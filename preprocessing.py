#! /usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import json
import matplotlib
from collections import Counter
import math

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np
from random import randint
from flashtext import KeywordProcessor
from math import log
from recipe import Recipe
from sklearn.feature_extraction import DictVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer


def read_categories():
    '''
    :return: categories for each recipe from file 'epi_r.csv'
    '''
    with open('epi_r.csv') as file:
        file_reader = csv.reader(file, delimiter=',')
        columns = None
        fst_row = True
        all_rows = []
        for row in file_reader:
            if fst_row:
                columns = row
                fst_row = False
                continue

            values = [columns[i] for i, value in enumerate(row) if value == '1.0']
            all_rows.append(values)

    return all_rows


def read_all_recipes():
    '''
    :return: parsed recipes from file 'full_format_recipes.json -> only first 500 recipes
    '''
    recipes = []
    count = 0
    with open('full_format_recipes.json') as f:
        for item in json.load(f):
            if item:
                if count == 500:
                    return recipes

                recipes.append(Recipe(
                    rating=item['rating'],
                    title=item['title'],
                    sodium=item['sodium'],
                    ingredients=item['ingredients'],
                    calories=item['calories'],
                    fat=item['fat'],
                    directions=item['directions'],
                    protein=item['protein'],
                    categories=item['categories'],
                    description=item['desc']
                ))
                count += 1

    return recipes


def count_of_occurrence(array):
    '''
    :param array: list of items
    :return: list of items and their occurrences
    '''
    a_set = set(array)
    results = []
    for item in a_set:
        results.append((item, array.count(item)))

    return results


def reduce_x_labels(labels, param):
    '''
    :return: only each "param"-th label on x axis
    '''
    reduced_labels = []
    for i, l in enumerate(labels):
        if i % param == 0:
            reduced_labels.append(l)

    return reduced_labels


def make_histogram(array, title, xlabel, ylabel, file, width_of_bar=0.3, rotation=50, reduced_params=1):
    labels, counts = np.unique(array, return_counts=True)

    plt.bar(labels, counts, width_of_bar, align='center', color='#fcb5cd')
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    maxfreq = max(counts)
    plt.ylim(ymax=maxfreq + maxfreq / 8)

    if reduced_params > 1:
        labels = reduce_x_labels(labels, reduced_params)

    plt.xticks(labels, rotation=rotation)
    plt.savefig(file)
    # plt.show()


def group_values(data, coef, threshold, threshold_value=None):
    '''
    Due to high amount of variance of values, divide values into groups in range e.g. -1 | 25 | 75 | 125 | 175 | ...

    :param data: list of values
    :param coef: size of groups
    :param threshold: values bigger than threshold are in one group
    :param threshold_value: group of values bigger than threshold has this value
    :return: values divided into groups
    item with None has value -1
    '''
    if not threshold_value:
        threshold_value = threshold

    data_with_scaling = []
    for l in data:
        if not l:
            data_with_scaling.append(-1)
            continue

        if l > threshold:
            l = threshold_value
        data_with_scaling.append((l // coef) * coef + coef / 2)

    return data_with_scaling


def rating_histogram(data):
    all_ratings = [recipe.get_rating() for recipe in data]
    all_ratings_without_none = [i if i != None else -1 for i in
                                all_ratings]  # rating can be None -> problem with drawing in plot
    make_histogram(all_ratings_without_none, "Rating", "score of rating", "number of occurrencies", "rating.png")


def fat_histogram(data):
    all_fats = [recipe.get_fat() for recipe in data]
    all_fats_with_scaling = group_values(all_fats, 50, 1100,
                                         1300)  # due to high amount of variance of values, divide values into groups
    make_histogram(all_fats_with_scaling, "Fat", "amount of fat", "number of occurrencies", "fat.png", width_of_bar=0.8,
                   rotation=90)


def calories_histogram(data):
    all_calories = [recipe.get_calories() for recipe in data]
    all_calories_with_scaling = group_values(all_calories, 200, 10000, 13000)
    make_histogram(all_calories_with_scaling, "Calories", "amount of calories", "number of occurrencies",
                   "calories.png", rotation=90, reduced_params=3)


def save_count_to_file(data, file):
    categorie_and_count = sorted(count_of_occurrence(data), key=lambda x: x[1], reverse=True)
    with open(file, mode="w+") as f:
        for categorie in categorie_and_count:
            f.write(categorie[0].encode('utf-8').decode() + ': ' + str(categorie[1]) + '\n')


def print_count(data):
    categories_and_count = sorted(count_of_occurrence(data), key=lambda x: x[1], reverse=True)
    print(categories_and_count)


def save_categories_to_file(data, file):
    '''
    :return: save categories and its count into file
    '''
    all_categories = []
    for rec in data:
        all_categories.extend(rec.get_categories())

    save_count_to_file(all_categories, file)


def keyword_ingredients_and_ther_count(data, count=False):
    keyword_processor = KeywordProcessor()
    keyword_processor.add_keyword_from_file('ingredients.txt')

    all_ingredients = []
    recipe_ingredients = []
    for recipe in data:
        for item in recipe.get_ingredients():
            ingrs = keyword_processor.extract_keywords(item)
            if len(ingrs) == 0:
                continue
            else:
                max_length_ingr = [max(ingrs, key=len)]
                recipe_ingredients.extend(max_length_ingr)
                all_ingredients.extend(max_length_ingr)

        recipe.set_ingredients(recipe_ingredients)
        recipe_ingredients = []

    if count:
        print_count(all_ingredients)

    with open("extracted_ingredients.txt", "w+") as f:
        for i in set(all_ingredients):
            f.write(i + "\r\n")


def random(data):
    return data[randint(0, len(data) - 1)]


def jaccard_coef(data, recipe):
    '''
    |A intersect B| / |A union B|
    1 == equality of A and B

    Compute Jaccard coef of recipe with all other recipes and save top 5 similar recipes to recipe's attribute
    '''
    jaccard_coeficients = []
    recipe_ingrs = set(recipe.get_ingredients())

    for another_recipe in data:
        another_ingrs = another_recipe.get_ingredients()
        if set(another_ingrs) == recipe_ingrs:  # the same recipe
            continue
        intersect = len(recipe_ingrs.intersection(set(another_ingrs)))
        union = len(recipe_ingrs.union(set(another_ingrs)))
        jaccard_coeficients.append((intersect / union, another_recipe))

    recipe.set_jaccard(sorted(jaccard_coeficients, key=lambda x: x[0], reverse=True)[:5])


def get_recipe_info_without_jaccard(recipe):
    return {
        'fat': recipe.get_fat(),
        'rating': recipe.get_rating(),
        'calories': recipe.get_calories(),
        'protein': recipe.get_protein(),
        'title': recipe.get_title(),
        'sodium': recipe.get_sodium(),
        'categories': recipe.get_categories(),
        'directions': recipe.get_directions(),
        'ingredients': recipe.get_ingredients(),
    }


def add_to_freq_dict(dict, key):
    if key in dict:
        dict[key] += 1
    else:
        dict[key] = 1


def freq_dict(data):
    list_of_dict = []
    main_dict = {}
    for recipe in data:
        dict = {}
        for ingr in recipe.get_ingredients():
            add_to_freq_dict(dict, ingr)
            add_to_freq_dict(main_dict, ingr)
        list_of_dict.append((dict, len(recipe.get_ingredients())))
    return list_of_dict, main_dict


def compute_tf(dicts):
    tf_scores = []
    for i, (dict, count) in enumerate(dicts):
        for k, v in dict.items():
            tf_scores.append({"recipe_index": i, "ingredient": k, "TF_Score": v / count})
    return tf_scores


def compute_idf(dicts, main_dict):
    idf_scores = []
    num_of_recipes = len(dicts)
    for i, (dict, _) in enumerate(dicts):
        for k, v in dict.items():
            idf_scores.append({"recipe_index": i, "ingredient": k, "IDF_Score": log(num_of_recipes / main_dict[k])})
    return idf_scores


def compute_tfidf(tf_scores, idf_scores):
    tfidf_scores = []
    for j in idf_scores:
        for i in tf_scores:
            if j["recipe_index"] == i["recipe_index"] and j["ingredient"] == i["ingredient"]:
                tfidf_scores.append({"recipe_index": i["recipe_index"],
                                     "ingredient": i["ingredient"],
                                     "TFIDF_Score": i["TF_Score"] * j["IDF_Score"]})
    return tfidf_scores


def compute_cos_similarity(tfidf_scores, recipes):
    transformed_tfidf = []
    last_recipe_index = 0
    new_dict = {}
    for dict in tfidf_scores:
        if dict["recipe_index"] == last_recipe_index:
            new_dict[dict["ingredient"]] = dict["TFIDF_Score"]
        else:
            transformed_tfidf.append(new_dict)
            last_recipe_index += 1
            new_dict = {}
            new_dict[dict["ingredient"]] = dict["TFIDF_Score"]
    transformed_tfidf.append(new_dict)

    dict_vectorizer = DictVectorizer().fit_transform(transformed_tfidf)
    print(str(dict_vectorizer))

    cosine_similarities = []
    for index1, i in enumerate(dict_vectorizer):
        print(index1)
        temp = []
        for index2, j in enumerate(dict_vectorizer):
            # if index1 == index2:
            #     temp.append(None)
            #     continue
            temp.append((cosine_similarity(i, j)[0][0], index2))

        # Get only the 5 most similar ones (first is always identity)
        recipes[index1].set_cosine(sorted(temp, key=lambda x: x[0], reverse=True)[1:16])
        cosine_similarities.append(temp)

    return cosine_similarities


def tf_idf_cos_sim(data):
    dicts, main_dict = freq_dict(data)
    # print("TF")

    tf_scores = compute_tf(dicts)
    # print("IDF")

    idf_scores = compute_idf(dicts, main_dict)
    # print("TF-IDF")

    tfidf_scores = compute_tfidf(tf_scores, idf_scores)

    # tfidf_scores = read_preprocessed_data("tf-idf.txt")
    with open("tf-idf.txt", "w+") as f:
        f.write(json.dumps(tfidf_scores, indent=4, ensure_ascii=False))
    #exit()

    print("COS")
    cos_similarity = compute_cos_similarity(tfidf_scores, all_recipes)
    return cos_similarity


def save_recipes_to_file(data, file):
    all_recipes = []
    for recipe in data:
        recipe_json = get_recipe_info_without_jaccard(recipe)
        recipe_json.update({
            'jaccard': [(jacc[0], get_recipe_info_without_jaccard(jacc[1])) for jacc in recipe.get_jaccard()],
        })

        recipe_json.update({
            'cosine': [(item[0].item(), get_recipe_info_without_jaccard(data[item[1]])) for item in recipe.get_cosine()]
        })
        all_recipes.append(recipe_json)

    with open(file, "w+") as f:
        f.write(json.dumps(all_recipes))


def read_preprocessed_data(file):
    with open(file, 'r') as f:
        return json.loads(f.read())


def statistics(all_recipes):
    rating_histogram(all_recipes)
    fat_histogram(all_recipes)  # TODO fix graph
    calories_histogram(all_recipes)  # TODO fix graph


def get_title_rating_ingr(recipe):
    return recipe.get_rating(), recipe.get_title(), recipe.get_ingredients()


def generate_tf_json(data, file):
    all_recipes = []
    with open('tf-idf.txt', 'r+') as f:
        ingr_json = json.loads(f.read())
        for i, recipe in enumerate(data):
            rating, title, ingrs = get_title_rating_ingr(recipe)
            recipe_json = {
                'rating': rating,
                'title': title,
            }
            ingredients = {}

            for item in ingr_json:
                if item['recipe_index'] == i:
                    ingredients[item['ingredient']] = item['TFIDF_Score']

            recipe_json['ingredients'] = ingredients
            all_recipes.append(recipe_json)

    with open(file, "w+") as f:
        f.write(json.dumps(all_recipes, indent=4, ensure_ascii=False))


def get_cosine(vec1, vec2):
     """
     :param vec1: {'sugar': 1, 'olive oil': 1, 'chocolate': 1, 'flour': 1}
     :param vec2: {'tomato': 1, 'olive oil': 1, 'oregano': 1, 'flour': 1}
     :return:
     """
     intersection = set(vec1.keys()) & set(vec2.keys())  # {'olive oil', 'flour'}
     numerator = sum([vec1[x] * vec2[x] for x in intersection])  # sucin poctu vyskytov pre kazde slovo z prieniku (a nakoniec suma)

     sum1 = sum([vec1[x]**2 for x in vec1.keys()])  # (pocet vyskytov)^ 2 pre kazde slovo z vec1
     sum2 = sum([vec2[x]**2 for x in vec2.keys()])
     denominator = math.sqrt(sum1) * math.sqrt(sum2)

     if not denominator:
        return 0.0
     else:
        return float(numerator) / denominator

def text_to_vector(text):
    """
    'sugar, olive oil, chocolate, flour' -> {'sugar': 1, 'olive oil': 1, 'chocolate': 1, 'flour': 1}
    :param text: string contains ingredients (like 'sugar, olive oil, chocolate, flour')
    :return: dict - for each word in string number of its occurrence
    """
    words = text.split(',')

    return Counter(words)

def cosine_without_tfidf():
    all_ingredients = []
    all_recipes = read_preprocessed_data("recipes.txt")
    # z listu ingrediencii spravim string: ['oil', 'sugar', 'bread'] -> 'oil, sugar, bread'
    for recipe in all_recipes:
        all_ingredients.append(', '.join(recipe['ingredients']))

    vectors = []
    # pre kazdé slovo napocitam pocet vyskytov
    for rec in all_ingredients:
        vectors.append(text_to_vector(rec))

    top_for_all = {}
    # pre kazde ingrediencie poctam podobnost so vsetkymi ostatnymi ingrds
    for i in range(len(all_ingredients)):
        cosines = {}
        for j in range(len(all_ingredients)):
            cosine = get_cosine(vectors[i], vectors[j])
            cosines[j] = cosine   # vytvaram dict - 'index': 'hodnota_cos_sim' (aby som pri triedeni mala zachovane indexy)

        # usporiadam dict podla hodnot a ulozim top 10 podobnych receptov do zoznamu (index, hodnota)
        # top_10 = [(473, 0.387), (106, 0.365), (19, 0.335), (151, 0.335), (287, 0.335), (487, 0.335), (90, 0.316), (174, 0.316), (238, 0.316)]
        # top_for_all = {'16': [(473, 0.387), (106, 0.365), (19, 0.335), (151, 0.335), (287, 0.335), (487, 0.335), (90, 0.316), (174, 0.316), (238, 0.316)]}
        top_10 = [(k, cosines[k]) for k in sorted(cosines, key=cosines.get, reverse=True)][1:10]  # first is identity
        top_for_all[i] = top_10

    # vypis (subor cosine_without_tfidf.txt)
    # for rec, top in top_for_all.items():
    #     print(str(rec) + ': ' + str(top))
    #     print(all_ingredients[rec])
    #     print(all_ingredients[top[0][0]])
    #     print('-----')

def get_all_permut_lists(data):
    """
    zoznam vsetkych ingrediencii (ingrediencie pre 1 recept su repre 1 stringom) -> kazdy prvok zoznamu musi byt raz na prvom mieste (taka neuplna permutacia)
    :param data: ['oil, sugar', 'chocolate', 'salt, oil']
    :return: [ ['oil, sugar', 'chocolate', 'salt, oil'], ['chocolate', 'salt, oil', 'oil, sugar'], ['salt, oil', 'oil, sugar', 'chocolate'] ]
    """
    all_lists = []
    for i in range(len(data)):
        all_lists.append(data[i:] + data[:i])

    return all_lists

def cosine_with_tfidf():
    all_ingredients = []
    all_recipes = read_preprocessed_data("recipes.txt")

    # z listu ingrediencii spravim string: ['oil', 'sugar', 'bread'] -> 'oil, sugar, bread'
    for recipe in all_recipes:
        all_ingredients.append(', '.join(recipe['ingredients']))

    # prvy prvok berie ako ten, voci ktoremu sa pocita podobnost, preto permutacia
    all_ingredients_permut = get_all_permut_lists(all_ingredients)

    for i, ingrs in enumerate(all_ingredients_permut):
        # Construct the training set as a list - zoznam vsetkych ingrediencii receptov
        train_set = ingrs

        # Set up the vectoriser
        tfidf_vectorizer = TfidfVectorizer()

        # Apply the vectoriser to the training set
        tfidf_matrix_train = tfidf_vectorizer.fit_transform(train_set)

        # print("Similarity Score [" + str(i) + "]")
        cosine_results_list = cosine_similarity(tfidf_matrix_train[0:1], tfidf_matrix_train)[0].tolist() # poradie ako v train_set
        cosine_results_dict = {}

        # z listu vysledkov [0.2, 0.62, 0.3] -> dict s poradim: {1: 0.2, 2: 0.62, 3: 0.3}
        for j, cos in enumerate(cosine_results_list):
            cosine_results_dict[j] = cos

        top_10 = [(k, cosine_results_dict[k]) for k in
                  sorted(cosine_results_dict, key=cosine_results_dict.get, reverse=True)][1:10]  # first is identity

        # vypis (subor cosine_with_tfidf.txt)
        # print(top_10)
        # print(all_ingredients[i])
        # print(all_ingredients[top_10[0][0]])
        # print('----')

if __name__ == '__main__':

    # all_recipes = read_all_recipes()

    # statistics(all_recipes)

    # save_categories_to_file(all_recipes, "categories.txt")


    cosine_without_tfidf()
    cosine_with_tfidf()


    # all_ingredients = []
    # all_recipes = read_preprocessed_data("recipes.txt")
    # for recipe in all_recipes:
    #     all_ingredients.append(', '.join(recipe['ingredients']))


    # keyword_ingredients_and_ther_count(all_recipes)
    # generate_tf_json(all_recipes, 'tfidf_info.txt')

    # random_recipe = random(all_recipes)

    # for recipe in all_recipes:
    #     jaccard_coef(all_recipes, recipe)

    #
    # r = Recipe(rating=4, title='Avocado bread', sodium=238,
    #            ingredients=['avocado', 'pepper', 'tomato', 'egg', 'milk'],
    #            calories=120, fat=23, directions=[], protein=None, categories=None, description=None)
    #
    # s = Recipe(rating=4, title='Avocado dream', sodium=238,
    #            ingredients= ['avocado', 'milk', 'egg', 'chocolate', 'pepper'],
    #            calories=120, fat=23, directions=[], protein=None, categories=None, description=None)
    #
    # p = Recipe(rating=4, title='Avocado dream', sodium=238,
    #            ingredients=['cheese', 'egg', 'chocolate', 'cookies'],
    #            calories=120, fat=23, directions=[], protein=None, categories=None, description=None)
    #
    # q = Recipe(rating=4, title='Avocado dream', sodium=238,
    #            ingredients=['butter', 'onion', 'chocolate'],
    #            calories=120, fat=23, directions=[], protein=None, categories=None, description=None)
    #
    # all_recipes = [r, s, p, q]


    # for rec in all_recipes[:25]:
    #     print (rec.get_ingredients())


    # cos_sim = tf_idf_cos_sim(all_recipes)
    # print (cos_sim)

    # with open("cosine_sim.txt", "w+") as f:
    #     f.write(json.dumps(cos_sim, indent=4, ensure_ascii=False))
    #
    #
    # save_recipes_to_file(all_recipes, 'recipes.txt')
